# FULLSTACK MONGODB EXPRESS REACT NODEJS MERN (BACKEND)

[BackEnd - (server)](https://gitlab.com/react_development1/fullstack_mongodb_express_react_nodejs_mern_bookapp)

### To run the app (server)

##### Make sure you are in /fullstack_mongodb_express_react_nodejs_mern_bookapp/ directory & type the following command

```sh
$ npm install
$ npm run app
```

### You will find the front end part (FRONTEND REACT BOOKAPP) here.

[FrontEnd - (Client)](https://gitlab.com/react_development1/fullstack_mongodb_express_react_nodejs_mern_bookapp/-/tree/main/frontend_react_bookapp)

### To run the app (client)

##### Make sure you are in /frontend_react_bookapp/ directory & type the following command

```sh
$ npm install
$ npm start
```
